using System;
using System.Collections.Generic;
using System.Linq;

public class Creature : MapObject
{
    public enum DesireType {Food, Nest};
    public DesireType Desire;
    public enum ActionType {Move, Eat, Sleep}
    public ActionType Action;
    private Location Destination;
    public float Energy;
    public float Size;
    public float Speed;
    public float Sense;
    public int HungerLimit;
    public int Age;
    public int Generation;
    public int Children;
    enum Direction {None, North, East, South, West}
        
    private Creature()
    {

    }

    public static Creature Create()
    {
        Creature creature = new Creature();

        creature.Location       = Location.GetRandom();
        creature.Age            = 0;
        creature.Generation     = 0;
        creature.Children       = 0;
        creature.Energy         = WorldParameters.Energy;
        creature.Size           = WorldParameters.Size;
        creature.Speed          = WorldParameters.Speed;
        creature.Sense          = WorldParameters.Sense;
        creature.HungerLimit    = WorldParameters.HungerLimit;
        creature.Desire         = DesireType.Food;
        creature.Destination    = creature.explore();

        return creature;
    }

    public void activate()
    {
        for (int m = 0; m < this.Speed; m++)
            this.playTurn();        
    }

    private void playTurn()
    {
        // When energy below 0 die :(
        if (this.Energy <= 0)
            return;
        
        if (this.Energy > (WorldParameters.Energy * ((float)this.HungerLimit / 100F)))
            // When enough energy, find a sleeping spot
            this.Desire = DesireType.Nest;
        else
            // Else look for more food
            this.Desire = DesireType.Food;

        // Determine action based on need
        switch (this.Desire)
        {
            case DesireType.Food:
                if (this.Location.getTile().HasFood)
                    this.Action = ActionType.Eat;
                else
                {
                    Food food = this.scanForFood();
                    if (food != null)
                        this.Destination = food.Location;
                    this.Action = ActionType.Move;
                }
                break;

            case DesireType.Nest:
                if (this.Location.getTile().HasNest)
                    this.Action = ActionType.Sleep;
                else
                {
                    Nest nest = this.scanForNest();
                    if (nest != null)
                        this.Destination = nest.Location;                    
                    this.Action = ActionType.Move;
                }
                break;
        }

        //Perform action (Split for readability)
        switch (this.Action)
        {
            case ActionType.Eat:
                this.eat(this.Location.getTile().getFood());
                break;

            case ActionType.Sleep:
                this.sleep(this.Location.getTile().getNest());
                break;

            case ActionType.Move:
                if (this.Location.isAt(this.Destination))
                    this.Destination = this.explore();

                this.move(this.Destination);
                break;
        }
        
    }

    public Creature mate()
    {
        Random random = new Random();
        
        if (random.Next(100) < WorldParameters.ReproductionChance)
        {
            //Score!
            return this.reproduce();
        }

        //Tough luck
        return null;        
    }

    protected Creature reproduce()
    {
        Creature child      = Creature.Create();

        child.Generation    = this.Generation + 1;
        child.Location      = this.Location.getCopy();

        //Inherite and mutate
        child.Size          = mutate(this.Size,         WorldParameters.SizeMutationProbability,           WorldParameters.SizeMutationIncrement);
        child.Speed         = mutate(this.Speed,        WorldParameters.SpeedMutationProbability,          WorldParameters.SpeedMutationIncrement);
        child.Sense         = mutate(this.Sense,        WorldParameters.SenseMutationProbability,          WorldParameters.SenseMutationIncrement);
        child.HungerLimit   = mutate(this.HungerLimit,  WorldParameters.HungerLimitMutationProbability,    WorldParameters.HungerLevelMutationIncrement);

        this.Children += 1;

        return child;
    }

    private float mutate(float _value, int _mutationProbability, int _mutationIncrement)
    {
        float result = _value;

        Random random = new Random();
        int forBetterOrWorse = random.Next(100) < 50 ? 1 : -1;            

        // Do we mutate?
        if (random.Next(100) < _mutationProbability)
        {
            //Mutate!
            result = _value + (_mutationIncrement * forBetterOrWorse);
        }

        if (result < 1)
            result = 1;

        return result;
    }

    private int mutate(int _value, int _mutationProbability, int _mutationIncrement)
    {
        return (int)mutate((float)_value, _mutationProbability, _mutationIncrement);
    }

    protected void move(Location _destination)
    {
        Direction directionLat;
        Direction directionLon;

        //Stay on the straight and narrow
        this.Destination = _destination;

        //Find closest lateral direction
        int distance = this.Location.y - _destination.y;

        if (distance > 0)
            directionLat = distance > WorldParameters.Height / 2    ? Direction.South : Direction.North;
        else if (distance < 0)
            directionLat = distance*-1 > WorldParameters.Height / 2 ? Direction.North : Direction.South;
        else
            directionLat = Direction.None;

        //Find closest longitudinal
        distance = this.Location.x - _destination.x;

        if (distance > 0)
            directionLon = distance > WorldParameters.Width / 2    ? Direction.East : Direction.West;
        else if (distance < 0)
            directionLon = distance*-1 > WorldParameters.Width / 2 ? Direction.West : Direction.East;
        else
            directionLon = Direction.None;

        this.move(directionLon, directionLat);
    }
    private void move(Direction _directionLon, Direction _directionLat)
    {
        //Move lon
        switch (_directionLon)
        {
            case Direction.East:
                this.Location.x++;
                break;

            case Direction.West:
                this.Location.x--;
                break;                
        }

        //Move lat
        switch (_directionLat)
        {
            case Direction.North:
                this.Location.y--;
                break;

            case Direction.South:
                this.Location.y++;
                break;                
        }

        //Handle edges
        this.Location.x = Location.ConvertWidth(this.Location.x);
        this.Location.y = Location.ConvertHeight(this.Location.y);

        //Tire
        this.tire();
    }

    private MapObject scan(DesireType _desire)
    {
        int scanDistance = (int)this.Sense;

        for (int s = 1; s <= scanDistance; s++) //TODO: Improve - At the moment we re-scan already scanned tiles
            for (int y = this.Location.y - s; y <= this.Location.y + s; y++)
                for (int x = this.Location.x - s; x <= this.Location.x + s; x++)
                {
                    //Skip already scanned tiles (Scan only the edges)
                    if (    (y > this.Location.y - s && y < this.Location.y + s) 
                        &&  (x > this.Location.x - s && x < this.Location.x + s))
                    {
                        // Skip to right edge. We use -1 since the for loop will still increment x by one
                        x = this.Location.x + s - 1; 
                        continue;
                    }

                    Tile tile = Environment._obj.World[Location.ConvertWidth(x), Location.ConvertHeight(y)];

                    switch (_desire)
                    {
                        case DesireType.Food:
                            if (tile.HasFood) return tile.getFood();
                            break;

                        case DesireType.Nest:
                            if (tile.HasNest) return tile.getNest();
                            break;                            
                    }
                }

        return null;
    }

    protected Food scanForFood()
    {        
        return (Food)scan(DesireType.Food);
    }

    protected Nest scanForNest()
    {        
        return (Nest)scan(DesireType.Nest);
    }

    protected void tire()
    {
        float movementPenalty   = 3;
        float speedPenalty      = 2 + (this.Speed / 2);
        float sensePenalty      = 2 + (this.Sense / 2);
        float sizePenalty       = 3 + (this.Size  / 2);

        float energyLoss = movementPenalty + speedPenalty + sensePenalty + sizePenalty;

        this.Energy -= energyLoss;
    }

    protected void eat(Food _food)
    {
        //Consume food
        this.Energy += _food.EnergyValue;

        //Cannot over-eat
        if (this.Energy > WorldParameters.Energy)
            this.Energy = WorldParameters.Energy;

        //Remove food
        Environment._obj.RemoveFood(_food);
        
    }

    protected void sleep(Nest _nest)
    {
    }
    
    internal void wakeUp()
    {
        //Age one day
        this.Age++;

        //If I got some sleep in a nest
        if (this.Action == ActionType.Sleep)
            this.Energy += WorldParameters.EnergyChangeOvernight_Nest;
        else
        //If I spend the night outside
            this.Energy += WorldParameters.EnergyChangeOvernight_Exposed;

        //Debug info:
        Console.WriteLine($"[{Environment._obj.Day}:{this.Location.x:D3},{this.Location.y:D3}] Age:{this.Age:D2}; Gen:{this.Generation:D2}; Kids:{this.Children} [Sn{this.Sense}/Sp{this.Speed}/Sz{this.Size}/Hl{this.HungerLimit}] Energy:{this.Energy.ToString("0.##")} Act:{this.Action};");            
    }

    protected Location explore()
    {
        return Location.GetRandom();
    }
}