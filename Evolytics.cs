using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

public class Evolytics
{
    private Dictionary<int, EvolyticsDay> dayData = new Dictionary<int, EvolyticsDay>();

    public Evolytics()
    {
       // var x = Environment._obj.World;
        Environment._obj.BeforeNewDayEvent += beforeNewDay;
    }

    public void beforeNewDay(object _sender, EventArgs _eventArgs)  
    {  
        //Get average creature for the last day
        Environment environment = (Environment)_sender;

        // TODO: Average tihs over nth days too? :)
        if (environment.Day % WorldParameters.LogEveryNthDay == 0
            && Environment._obj.PopulationHandler.Population.Count > 0)
        {
            EvolyticsDay evolyticsDay = new EvolyticsDay();

            evolyticsDay.AgeAvg          = Environment._obj.PopulationHandler.Population.Average(c => c.Age);
            evolyticsDay.SpeedAvg        = Environment._obj.PopulationHandler.Population.Average(c => c.Speed);
            evolyticsDay.SenseAvg        = Environment._obj.PopulationHandler.Population.Average(c => c.Sense);
            evolyticsDay.SizeAvg         = Environment._obj.PopulationHandler.Population.Average(c => c.Size);
            evolyticsDay.ChildrenAvg     = Environment._obj.PopulationHandler.Population.Average(c => c.Children);
            evolyticsDay.HlAvg           = Environment._obj.PopulationHandler.Population.Average(c => c.HungerLimit);
            evolyticsDay.EnergyAvg       = Environment._obj.PopulationHandler.Population.Average(c => c.Energy);

            evolyticsDay.SpeedMin        = Environment._obj.PopulationHandler.Population.Min(c => c.Speed);
            evolyticsDay.SenseMin        = Environment._obj.PopulationHandler.Population.Min(c => c.Sense);
            evolyticsDay.SizeMin         = Environment._obj.PopulationHandler.Population.Min(c => c.Size);
            evolyticsDay.HlMin           = Environment._obj.PopulationHandler.Population.Min(c => c.HungerLimit);
            evolyticsDay.EnergyMin       = Environment._obj.PopulationHandler.Population.Min(c => c.Energy);

            evolyticsDay.AgeMax          = Environment._obj.PopulationHandler.Population.Max(c => c.Age);
            evolyticsDay.SpeedMax        = Environment._obj.PopulationHandler.Population.Max(c => c.Speed);
            evolyticsDay.SenseMax        = Environment._obj.PopulationHandler.Population.Max(c => c.Sense);
            evolyticsDay.SizeMax         = Environment._obj.PopulationHandler.Population.Max(c => c.Size);
            evolyticsDay.ChildrenMax     = Environment._obj.PopulationHandler.Population.Max(c => c.Children);
            evolyticsDay.HlMax           = Environment._obj.PopulationHandler.Population.Max(c => c.HungerLimit);
            evolyticsDay.EnergyMax       = Environment._obj.PopulationHandler.Population.Max(c => c.Energy);

            evolyticsDay.PopulationSize  = Environment._obj.PopulationHandler.Population.Count;
            evolyticsDay.FoodSupply      = Environment._obj.FoodSupplyHandler.FoodSupply.Count;
            evolyticsDay.NestCount       = Environment._obj.NestsHandler.Nests.Count;

            dayData.Add(environment.Day, evolyticsDay);
        }
    }  

    public void printDailyCreatureStats()
    {
        if (dayData.Count > 0)
        {
            var csv = new StringBuilder();

            //TODO: Replace with toCSVHeaderString() or something on abstract base class
            csv.AppendLine($"Day, {EvolyticsDay.toCSVStringHeader()}");

            foreach (KeyValuePair<int, EvolyticsDay> entry in dayData)
            {
                int day = entry.Key;
                EvolyticsDay evolyticsDay = entry.Value;

                //TODO: Replace with toCSVString() or something on abstract base class
                csv.AppendLine($"{day}, {evolyticsDay.toCSVString()}");
            }

            File.WriteAllText(WorldParameters.CsvFilePath, csv.ToString());
            Console.WriteLine($"Data written to '{WorldParameters.CsvFilePath}'");
            Console.WriteLine(dayData.Count == WorldParameters.Days ? $"Colony surived set period" : $"Colony went extinct on day {dayData.Count}");
        }
    }
}