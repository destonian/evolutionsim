﻿using System;

namespace evolutionsim
{
    class Program
    {
        static void Main(string[] args)
        {
            // Log data.
            Evolytics evolytics = new Evolytics();
            //TODO: Renderer here too?

            // Select naturally!
            new SimulationHandler().run();

            // Print data.
            evolytics.printDailyCreatureStats();
        }
    }
}
