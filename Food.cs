public class Food : MapObject
{
    public int EnergyValue;
    public bool IsAlive;

    private Food()
    {
    
    }

    public static Food Create()
    {
        Food food = new Food();
        
        food.Location       = Location.GetRandom();
        food.EnergyValue    = WorldParameters.FoodEnergy;
        food.IsAlive        = true;

        return food;
    }

}