public static class WorldParameters
{
    // World
    public static int       Width = 200;                            // Horizontal world tile count
    public static int       Height = 200;                           // Vertical world tile count
    public static int       Days = 1000;                            // Amount of days to run the simulation
    public static int       LengthOfDay = 24;                       // Amount of hours (moves) per day
    public static int       Population = 50;                        // Population size
    public static int       FoodQuantityInitial = 2000;             // Quantity of edibles on map
    public static float     FoodQuantityDailyChange = -10.00F;      // Change in quantity of food that gets respawned every morning
    public static int       MinimumFoodSupply = 200;                // Daily food will never drop below this
    public static int       MaximumFoodSupply = 10000;              // Daily food will never grow beyond this
    public static int       Nests = 100;                            // Quantity of nests on map

    // Food
    public static int       FoodEnergy = 50;                        // Initial new food energy level

    // Nests
    public static float     EnergyChangeOvernight_Nest = -5;        // Night penalty
    public static float     EnergyChangeOvernight_Exposed = -20;    // Energy penalty for not making it to a nest

    // Creatures
    public static float     Energy = 150;                           // Starting energy level
    public static int       Speed = 3;                              // Starting speed level
    public static int       Size = 5;                               // Starting size
    public static float     Sense = 2;                              // Starting sense range
    public static int       ReproductionChance = 80;
    public static int       SizeMutationIncrement = 1;              // Level change during mutation
    public static int       SizeMutationProbability = 80;           // Chance that mutation will happen
    public static int       SpeedMutationIncrement = 1;             // Level change during mutation
    public static int       SpeedMutationProbability = 80;          // Chance that mutation will happen
    public static int       SenseMutationIncrement = 1;             // Level change during mutation
    public static int       SenseMutationProbability = 80;          // Chance that mutation will happen
    public static int       HungerLimit = 50;                       // Percentage of full energy under which food becomes a priority
    public static int       HungerLevelMutationIncrement = 2;       // Level change during mutation
    public static int       HungerLimitMutationProbability = 80;    // Chance that mutation will happen    

    // Data analysis
    public static string    CsvFilePath = "c:\\Temp\\simresult.csv";// CSV file where results will be written to
    public static int       LogEveryNthDay = 1;                     //Only log every nth day to the file (Set to 1 if every day should be logged)    
}