using System.Collections.Generic;

public class FoodSupplyHandler
{
    private List<Food> foodSupply;
    private float foodSupplyYesterday;

    public FoodSupplyHandler()
    {
        foodSupplyYesterday = WorldParameters.FoodQuantityInitial;
    }

    public List<Food> FoodSupply
    {
        get
        {
            if (foodSupply == null)
                this.growFood(WorldParameters.FoodQuantityInitial);
                
            return foodSupply;
        }
    }

    public void growFood(float _foodQuantityChange)
    {
        if (foodSupply == null)
            foodSupply = new List<Food>();

        float foodSupplyToday = foodSupplyYesterday + _foodQuantityChange;

        if (foodSupplyToday > WorldParameters.MaximumFoodSupply)
            foodSupplyToday = WorldParameters.MaximumFoodSupply;

        if (foodSupplyToday < WorldParameters.MinimumFoodSupply)
            foodSupplyToday = WorldParameters.MinimumFoodSupply;
            
        for (int x = 0; x < foodSupplyToday; x++)
            foodSupply.Add(Food.Create());

        foodSupplyYesterday = foodSupplyToday;
    }
}