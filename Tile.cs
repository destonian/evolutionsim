using System.Collections.Generic;
using System.Linq;

public class Tile
{
    public bool HasFood 
    {
        get {return (FoodSupply != null && FoodSupply.Count > 0);}
    }
    
    public bool HasNest
    {
        get {return (Nests != null && Nests.Count > 0);}
    }

    public bool HasCreature
    {
        get {return (Creatures != null && Creatures.Count > 0);}
    }

    public Food getFood()
    {
        if (this.HasFood)
            return this.FoodSupply.FirstOrDefault();
        else
            return null;
    }

    public Nest getNest()
    {
        if (this.HasNest)
            return this.Nests.FirstOrDefault();
        else
            return null;
    }

    public List<Food> FoodSupply;
    public List<Nest> Nests;
    public List<Creature> Creatures;
    
    public Tile()
    {

    }
}