using System.Collections.Generic;

public class NestsHandler
{
    private List<Nest> nests;

    public List<Nest> Nests
    {
        get
        {
            if (nests == null)
                this.digNests(WorldParameters.Nests);

            return nests;
        }
    }

    public void digNests(int _nestQuantity)
    {
        nests = new List<Nest>();

        for (int x = 0; x < _nestQuantity; x++)
            nests.Add(Nest.Create()); 
    }
}