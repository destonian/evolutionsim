using System;

public class Location
{
    public int x;
    public int y;

    private Location()
    {

    }

    public static Location Create(int _x, int _y)
    {
        return new Location {x = _x, y = _y};
    }

    public static Location GetRandom()
    {
        Random random = new Random();
        Location location = new Location();
        
        location.x = random.Next(WorldParameters.Width);
        location.y = random.Next(WorldParameters.Height);
        
        return location;
    }

    public static int ConvertWidth(int _x)
    {
        if (_x < 0)
            return WorldParameters.Width + _x;
        
        if (_x >= WorldParameters.Width)
            return _x - WorldParameters.Width;

        return _x;
    }
    
    public static int ConvertHeight(int _y)
    {
        if (_y < 0)
            return WorldParameters.Height + _y;
        
        if (_y >= WorldParameters.Height)
            return _y - WorldParameters.Height;

        return _y;
    }    

    public Tile getTile()
    {
        return Environment._obj.World[this.x, this.y];
    }

    public bool isAt(Location _location)
    {
        return (this.x == _location.x && this.y == _location.y);
    }

    internal Location getCopy()
    {
        return Location.Create(this.x, this.y);        
    }
}