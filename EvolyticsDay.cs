using System.Text;

public class EvolyticsDay
{
    public double AgeAvg;
    public double AgeMax;

    public double SizeAvg;
    public double SizeMax;
    public double SizeMin;


    public double SenseAvg;
    public double SenseMax;
    public double SenseMin;


    public double SpeedAvg;
    public double SpeedMax;
    public double SpeedMin;


    public double HlAvg;    
    public double HlMax;
    public double HlMin;

    public double ChildrenAvg;    
    public double ChildrenMax;

    public double EnergyAvg;    
    public double EnergyMax;
    public double EnergyMin;

    public int PopulationSize;
    public int FoodSupply;
    public int NestCount;

    public static string toCSVStringHeader()
    {        
        StringBuilder csvString = new StringBuilder();

        csvString.Append("AgeAvg,AgeMax,");
        csvString.Append("SpeedAvg,SpeedMin,SpeedMax,");
        csvString.Append("SenseAvg,SenseMin,SenseMax,");
        csvString.Append("SizeAvg,SizeMin,SizeMax,");
        csvString.Append("ChildrenAvg,ChildrenMax,");
        csvString.Append("HlAvg,HlMin,HlMax,");
        csvString.Append("EnergyAvg,EnergyMin,EnergyMax,");
        csvString.Append("PopCount,FoodCount,NestCount");

        return csvString.ToString();
    }
    public string toCSVString()
    {
        StringBuilder csvString = new StringBuilder();

        csvString.Append($"{this.AgeAvg},{this.AgeMax},");
        csvString.Append($"{this.SpeedAvg},{this.SpeedMin},{this.SpeedMax},");
        csvString.Append($"{this.SenseAvg},{this.SenseMin},{this.SenseMax},");
        csvString.Append($"{this.SizeAvg},{this.SizeMin},{this.SizeMax},");
        csvString.Append($"{this.ChildrenAvg},{this.ChildrenMax},");
        csvString.Append($"{this.HlAvg},{this.HlMin},{this.HlMax},");
        csvString.Append($"{this.EnergyAvg},{this.EnergyMin},{this.EnergyMax},");
        csvString.Append($"{this.PopulationSize},{this.FoodSupply},{this.NestCount}");

        return csvString.ToString();
    }
}