using System;
using System.Collections.Generic;
using System.Linq;
using static Creature;

public class PopulationHandler
{
    private List<Creature> population;
    public List<Creature> Population
    {
        get
        {
            if (population == null)
                this.generatePopulation(WorldParameters.Population);

            return population;
        }
    }

    public void generatePopulation(int _populationSize)
    {
        population = new List<Creature>();

        for (int x = 0; x < _populationSize; x++)
            population.Add(Creature.Create()); 
    }
    public void activatePopulation()
    {
        //Play one turn
        foreach (var creature in population)
        {
            creature.activate();
        }

        //Remove the corpses
        population.RemoveAll(c => c.Energy <= 0);
    }

    internal void wakeUpSleepingCreatures()
    {
        List<Creature> children = new List<Creature>();

        foreach (var creature in population)
        {
            creature.wakeUp();

            if (creature.Action == ActionType.Sleep)
            {
                Creature child = creature.mate();

                if (child != null)
                    children.Add(child);
            }
        }

        //Welcome the kids to the family
        population.AddRange(children);
    }
}