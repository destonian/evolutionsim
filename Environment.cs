using System;
using System.Collections.Generic;

public class Environment
{
    public static readonly Environment _obj = new Environment();
    public readonly Tile[,] World = new Tile[WorldParameters.Width, WorldParameters.Height];
    public readonly NestsHandler NestsHandler;
    public readonly FoodSupplyHandler FoodSupplyHandler;
    public readonly PopulationHandler PopulationHandler;
    private int day = 0;
    public int Day
    {
        get 
        {
            return day;
        }
    }
                
    private Environment() 
    {
        // Instantiate handler classes.
        PopulationHandler   = new PopulationHandler();
        NestsHandler        = new NestsHandler();
        FoodSupplyHandler   = new FoodSupplyHandler();        
    }

    public void initialize()
    {
        // Add map objects.
        PopulationHandler.generatePopulation(WorldParameters.Population);
        FoodSupplyHandler.growFood(WorldParameters.FoodQuantityDailyChange);
        NestsHandler.digNests(WorldParameters.Nests);

        // Generate map tiles and place map objects.
        generateWorld();
        addObjectsToWorld();
        addCreaturesToWorld();
    }
    
    public delegate void BeforeNewDayEventHandler(object _sender, EventArgs _eventArgs);
    public event BeforeNewDayEventHandler BeforeNewDayEvent;
    
    public void newDay()
    {
        if (PopulationHandler.Population.Count == 0)
            return;

        //Trigger delegate
        if (BeforeNewDayEvent != null)
            BeforeNewDayEvent(this, null);
        
        //Change the calendar
        day++;

        //Grow new food
        FoodSupplyHandler.growFood(WorldParameters.FoodQuantityDailyChange);
        NestsHandler.Nests.Clear();
        NestsHandler.digNests(WorldParameters.Nests);
        PopulationHandler.wakeUpSleepingCreatures();

        //Reset world
        this.generateWorld();
        this.addObjectsToWorld();
        this.addCreaturesToWorld();
    }

    public void newHour()
    {
        PopulationHandler.activatePopulation();
        this.addCreaturesToWorld();
    }

    private void generateWorld()
    {
        // Create all floor tiles
        for (int x = 0; x < WorldParameters.Width; x++)
            for (int y = 0; y < WorldParameters.Height; y++)
        {
            Tile tile = new Tile();
            World[x,y] = tile;
        }
    }

    private void addObjectsToWorld()
    {
        // Add food
        foreach (Food food in FoodSupplyHandler.FoodSupply)
        {
            Tile tile = World[food.Location.x, food.Location.y];

            if (tile.FoodSupply == null)
                tile.FoodSupply = new List<Food>();

            //Attach to tile
            tile.FoodSupply.Add(food);
        }

        // Add nests
        foreach (var nest in NestsHandler.Nests)
        {
            Tile tile = World[nest.Location.x, nest.Location.y];

            if (tile.Nests == null)
                tile.Nests = new List<Nest>();

            //Attach to tile
            tile.Nests.Add(nest);
        }        
    }

    private void addCreaturesToWorld()
    {
        //Remove old locations
        for (int x = 0; x < WorldParameters.Width; x++)
            for (int y = 0; y < WorldParameters.Height; y++)
        {
            World[x,y].Creatures = null;
        }

        // Add creatures
        foreach (var creature in PopulationHandler.Population)
        {
            Tile tile = creature.Location.getTile();

            if (tile.Creatures == null)            
                tile.Creatures = new List<Creature>();

            //Attach to tile
            tile.Creatures.Add(creature);
        }   
    }

    public void RemoveFood(Food _food)
    {
        FoodSupplyHandler.FoodSupply.Remove(_food);
        _food.Location.getTile().FoodSupply.Remove(_food);
    }

    public void AddCreature(Creature child)
    {
        PopulationHandler.Population.Add(child);
    }
}