using System;

public class Renderer
{
    public static void DrawWorld()
    {
        string graphicLineRowHeader = "";

        // Header
        for (int x = 0; x < WorldParameters.Width; x++)
            graphicLineRowHeader += $"   {x:D2} ";

        Console.WriteLine($"  {graphicLineRowHeader}");

        // Body
        for (int y = 0; y < WorldParameters.Height; y++)
        {
            string graphicLine = "";

            for (int x = 0; x < WorldParameters.Width; x++)
            {
                graphicLine += "| ";
                graphicLine += (Environment._obj.World[x,y].HasFood      ? "F" : ".");
                graphicLine += (Environment._obj.World[x,y].HasNest      ? "N" : ".");
                graphicLine += (Environment._obj.World[x,y].HasCreature  ? "C" : ".");
                graphicLine += " ";
            }
            graphicLine += "|";
            Console.WriteLine($"{y:D2}{graphicLine}{y:D2}");
        }

        // Footer
        Console.WriteLine($"  {graphicLineRowHeader}");
    }
}