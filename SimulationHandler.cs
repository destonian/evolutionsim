public class SimulationHandler
{
    private Environment environment = Environment._obj;
    
    public void run()
    {
        // Initialize environment.
        environment.initialize();

        // Loop for days.
        for (int day = 0; day < WorldParameters.Days; day++)
        { 
            //Start new day.
            environment.newDay();

            // Loop for hours.
            for (int hour = 0; hour < WorldParameters.LengthOfDay; hour++)
            {
                //Console.WriteLine($"Day {day} {hour}:00 ->");
                // Start new hour.
                environment.newHour();
                //Renderer.DrawWorld();    
            }
        }
    }
}
