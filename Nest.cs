public class Nest : MapObject
{
    private Nest() {}

    public static Nest Create()
    {
        Nest nest = new Nest();
        nest.Location = Location.GetRandom();
        return nest;
    }
}